import request from "@/utils/request";
import { ResponseBasicModel } from "@/types/index.d";
import { AxiosPromise } from "axios";

// 枚举类
export function fetchLogin(data: object): Promise<ResponseBasicModel> {
  return request({
    url: "/login",
    method: "POST",
    data,
  });
}
