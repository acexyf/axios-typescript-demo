import { MockMethod } from "vite-plugin-mock";

export default [
  {
    url: "/api/login",
    method: "post",
    response: () => {
      return {
        success: true,
        data: {
          name: "aaaa",
        },
      };
    },
  },
] as MockMethod[];
