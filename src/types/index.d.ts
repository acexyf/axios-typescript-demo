export interface ResponseBasicModel<T> {
  success: boolean;
  msg: string;
  data: T;
}
